<?php
/**
 * Copyright © 2017 ThemeDimension.com
 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
//            $table->integer('user_id')->nullable()->unsigned();
            $table->integer('category_id')->unsigned();
            $table->integer('gender_id')->unsigned();
            $table->string('name');
            $table->string('color');
            $table->decimal('price');
            $table->text('image')->nullable();
            $table->text('siblings')->nullable();
            $table->softDeletes();


            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
//            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('gender_id')->references('id')->on('genders')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
