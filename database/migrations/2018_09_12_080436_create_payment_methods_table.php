<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_methods', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('code');
            $table->string('name');
            $table->timestamps();
        });

        $paymentMethods = [
            [
                'code' => 100,
                'name' => 'Stripe payment code',
            ],
            [
                'code' => 200,
                'name' => 'Paypal payment code',
            ],
            [
                'code' => 300,
                'name' => 'Custom payment code',
            ],
            [
                'code' => 400,
                'name' => 'No payment code',
            ],
        ];

        \App\PaymentMethod::insert($paymentMethods);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_methods');
    }
}
