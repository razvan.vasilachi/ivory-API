<?php
/**
 * Copyright © 2017 ThemeDimension.com
 */

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $password = Hash::make('toptal');

        $users = [
            'firstName'      => 'Name',
            'lastName'  => 'Last Name',
            'email'     => 'email@example.com',
            'phone'     => '074829256',
            'password'  => $password
        ];
        $usersInserted = \App\User::insert($users);
    }
}
