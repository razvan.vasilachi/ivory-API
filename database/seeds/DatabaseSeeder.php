<?php
/**
 * Copyright © 2017 ThemeDimension.com
 */

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(GenderTableSeeder::class);
//        $this->call(ProductTableSeeder::class);
//        $this->call(SizeTableSeeder::class);
        $this->call(LaratrustSeeder::class);
        $this->call(PassportSeeder::class);
    }
}
