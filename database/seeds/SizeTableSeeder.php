<?php
/**
 * Copyright © 2017 ThemeDimension.com
 */

use Illuminate\Database\Seeder;

class SizeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $sizes = [
            [
                'name' => 'XS',
                'product_id' => 1,
                'quantity' => '2',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
        ];

        $sizesInserted = \App\Size::insert($sizes);
    }
}
