<?php
/**
 * Copyright © 2017 ThemeDimension.com
 */

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return redirect()->route('admin.register');
});

Auth::routes();

Route::get('/password/reset', 'AuthAdmin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
Route::post('/password/email', 'AuthAdmin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::get('/password/reset/{token}', 'AuthAdmin\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('/password/reset', 'AuthAdmin\ResetPasswordController@reset');

// ADMIN LOGIN & LOGOUT
Route::prefix('admin')->group(function() {


    Route::get('/login', 'AuthController@getLogin')->name('admin.login');
    Route::post('/login', 'AuthController@postLogin')->name('admin.login');

    Route::get('/logout', 'AuthController@getLogout')->name('admin.logout');

    Route::get('/register', 'AuthController@getRegister')->name('admin.register');
    Route::post('/register', 'AuthController@postRegister')->name('admin.register');
});



Route::group(['middleware' => ['auth','role:administrator']], function() {
    Route::get('/admin', 'AuthController@getDashboard')->name('admin.dashboard');

    // PRODUCTS CRUD

    Route::get('/admin/products/', 'ProductController@index')->name('product.list');
    Route::get('/admin/products/add', 'ProductController@create')->name('product.add');
    Route::post('/admin/products/add', 'ProductController@store')->name('product.add');
    Route::get('/admin/products/delete/{id}', 'ProductController@destroy')->name('product.destroy');
    Route::get('/admin/products/{id}/edit', 'ProductController@edit')->name('product.edit');
    Route::put('/admin/products/{id}/update', 'ProductController@update')->name('product.update');

    Route::get('/admin/users/', 'UserController@index')->name('user.list');
    Route::get('/admin/users/{id}/edit', 'UserController@edit')->name('user.edit');
    Route::put('/admin/users/{id}/update', 'UserController@update')->name('user.update');
    Route::get('/admin/users/delete/{id}', 'UserController@destroy')->name('user.destroy');

    // LOGS
    Route::get('/admin/logs/', 'LogsController@index')->name('logs.index');
    // API ENDPOINTS
    Route::get('/admin/api-endpoints/', function() {
        return view ('api-endpoints');
    });

    Route::get('/admin/orders', 'OrderController@index')->name('orders.index');
    Route::get('/admin/orders/{id}', 'OrderController@show')->name('orders.show');
    Route::get('/admin/orders/delete/{id}', 'OrderController@delete')->name('orders.delete');
});
//Route::get('admin/products/', 'ProductController@index')->middleware('role:administrator');



