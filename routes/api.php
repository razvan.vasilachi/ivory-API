<?php
/**
 * Copyright © 2017 ThemeDimension.com
 */

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/login','API\PassportController@login');
Route::post('/register','API\PassportController@register');
Route::get('/logout','API\PassportController@logout');

Route::put('/users/{id}','API\PassportController@update');

Route::get('/users/{id}','API\PassportController@show');
Route::get('/users','API\PassportController@index');

Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('admin.password.reset');

Route::get('/products', 'API\ProductsController@getProducts')->name('api.product.list');
Route::get('/products/{id}', 'API\ProductsController@getProduct')->name('api.product');

Route::post('/order', 'API\OrderController@store')->name('api.order');
Route::get('/order/{id}', 'API\OrderController@getOrder')->name('api.order');
Route::get('/orders', 'API\OrderController@getOrders')->name('api.orders');

Route::post('/cartlist', 'API\CartController@store');
Route::put('/cartlist', 'API\CartController@update');
Route::get('/cartlist', 'API\CartController@index');