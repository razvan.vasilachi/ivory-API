<?php
/**
 * Copyright © 2017 ThemeDimension.com
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;


class Product extends Model
{
    use LogsActivity;
    use SoftDeletes;
    //
    protected $fillable = [
        'category_id',
        'gender_id',
        'name',
        'color',
        'price',
        'image',
        'siblings',
        'deleted_at'
    ];

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function gender(){
        return $this->belongsTo(Gender::class);
    }

    public function size(){
        return $this->hasMany(Size::class);
    }

    public function getSiblingsAttribute($value){
        return unserialize($value);
    }

    public function getLogNameToUse(string $eventName  ): string
    {
        return class_basename(static::class);
    }

    public function orders() {
        return $this->belongsToMany(Order::class)->withPivot('quantity', 'size_id', 'size_name');
    }
}
