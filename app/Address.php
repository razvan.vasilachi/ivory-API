<?php
/**
 * Copyright © 2017 ThemeDimension.com
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'city',
        'country',
        'district',
        'number',
        'postalCode',
        'street',
        'user_id'
    ];
    protected $hidden = [
        'created_at', 'updated_at', 'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
