<?php
/**
 * Copyright © 2017 ThemeDimension.com
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    //
    protected $fillable = [
        'name',
        'product_id',
        'quantity'
    ];

    public function products(){
        return $this->belongsTo(Product::class);
    }

}
