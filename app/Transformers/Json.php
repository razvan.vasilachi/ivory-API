<?php
/**
 * Copyright © 2017 ThemeDimension.com
 */

namespace App\Transformers;

class Json
{
    public static function response($data = null, $message = null)
    {
        return [
            'data'    => $data,
            'message' => $message,
        ];
    }
}