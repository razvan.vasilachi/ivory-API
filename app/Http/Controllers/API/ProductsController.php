<?php
/**
 * Copyright © 2017 ThemeDimension.com
 */

namespace App\Http\Controllers\API;

use App\Product;
use App\Size;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getProducts()
    {
        //
        $productsRaw = Product::all();

        $products    = collect([]);
//        $sizetest = Size::where('product_id',1)->get();
//        return $sizetest;
        foreach($productsRaw as $key=>$productRaw){
            $sizesRaw = $productRaw->size;
            $sizes = [];
            foreach($sizesRaw as $sizeRaw){
                $sizes [] = [
                    'id'        => $sizeRaw->id,
                    'name'      => $sizeRaw->name,
                    'quantity'  => floatval($sizeRaw->quantity)
                ];
            }
            $products->push(
                [
                    "productName"           => $productRaw->name,
                    "productId"             => $productRaw->id,
                    "productColor"          => $productRaw->color,
                    "productPrice"          => floatval($productRaw->price),
                    "sizes"                 => $sizes,
                    "productAddedDate"      => $productRaw->created_at->toDateString(),
                    "productGender"         => $productRaw->gender->name,
                    "productCategory"       => $productRaw->category->name,
                    "productImage"          => url("uploads/".$productRaw->image),
                    "sibilings"             => $productRaw->siblings
                ]
            );
        }
        return response()->json(['productList' => $products]);
        //return $productsRaw;
    }

    public function getProduct($id){

        $productRaw = Product::find($id);

        if(is_null($productRaw)){
            return response()->json([
                'message'   => 'Product not found'
            ],401);
        }

        $sizesRaw = $productRaw->size;
        $sizes = [];

        foreach($sizesRaw as $sizeRaw){
            $sizes [] = [
                'id'        => $sizeRaw->id,
                'name'      => $sizeRaw->name,
                'quantity'  => floatval($sizeRaw->quantity)
            ];
        }

        $product = collect([]);
        $product->push([
            "productName"           => $productRaw->name,
            "productId"             => $productRaw->id,
            "productColor"          => $productRaw->color,
            "productPrice"          => floatval($productRaw->price),
            "sizes"                 => $sizes,
            "productAddedDate"      => $productRaw->created_at->toDateString(),
            "productGender"         => $productRaw->gender->name,
            "productCategory"       => $productRaw->category->name,
            "productImage"          => url("uploads/".$productRaw->image),
            "sibilings"             => $productRaw->siblings
        ]);

        return $product;
    }
}
