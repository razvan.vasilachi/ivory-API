<?php
/**
 * Copyright © 2017 ThemeDimension.com
 */

namespace App\Http\Controllers\API;

use App\Order;
use App\Product;
use App\Size;
use App\User;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    //
    public $successStatus = 200;
    public function store(Request $request) {
        /*Cart::destroy();
        dd(Cart::content());*/
        $inputOrder = $request->all();
        $order = Order::create($inputOrder);

        $user = User::find($order->user_id);

        foreach (Cart::content() as $key=>$cart) {
            // decrease quantity
            $size = Size::where([
                ['product_id', '=', $cart->id],
                ['id', '=', $cart->options['size_id']]
            ])->first();

            $size->quantity = $size->quantity - $cart->qty;
            $size->update();

            $data[] = [
                'quantity' => $cart->qty,
                'product_id' => $cart->id,
                'order_id' => $order->id,
                'size_id'   => $cart->options['size_id'],
                'size_name'   => $cart->options['size_name'],
            ];
        }
        $order->amount = Cart::subtotal();
        $order->update();
        $order->products()->attach($data);

        activity()
            ->useLog('API')
            ->performedOn($user)
            ->causedBy($user)
            ->log('Create Order');

        return response()->json(['success' => 1], $this->successStatus);
    }

    public function getOrder($id) {
        $order = Order::find($id);

        $user = User::find($order->user_id);
        $address = User::find($order->user_id)->address;
//        dd($order->user->address);
        foreach (Cart::content() as $cart) {
            $items[] = [
                'quantity' => $cart->qty,
                'product_id' => $cart->id,
                'order_id' => $order->id,
                'size_id'   => $cart->options['size_id'],
                'size_name'   => $cart->options['size_name'],
            ];
        }
        $data = [
            'id' => $order->id,
            'address' => [
                'city' => $address->city,
                'country' => $address->country,
                'district' => $address->district,
                'number' => $address->number,
                'postalCode' => $address->postalCode,
                'street' => $address->street,
            ],
            'contact' => [
                'firstName' => $user->firstName,
                'lastName' => $user->lastName,
                'email' => $user->email,
                'phone' => $user->phone,
            ],

            'user_id' => $order->user_id,
            'amount' => $order->amount,
            'items' => $items
//            'payment_method_id' => $order->paymentMethod
        ];

        $data = collect($data);
        return response()->json(['response' => $data], $this->successStatus);
    }

    public function getOrders() {
        $orders = Order::all();
        $orderList = collect([]);

        foreach ($orders as $order) {
            $user = User::find($order->user_id);
            $address = User::find($order->user_id)->address;

            $data = [
                'id' => $order->id,
                'address' => [
                    'city' => $address->city,
                    'country' => $address->country,
                    'district' => $address->district,
                    'number' => $address->number,
                    'postalCode' => $address->postalCode,
                    'street' => $address->street,
                ],
                'contact' => [
                    'firstName' => $user->firstName,
                    'lastName' => $user->lastName,
                    'email' => $user->email,
                    'phone' => $user->phone,
                ],

                'user_id' => $order->user_id,
                'amount' => $order->amount
            ];

            $orderList->push($data);
        }

        return response()->json(['response' => $orderList], $this->successStatus);
    }
}
