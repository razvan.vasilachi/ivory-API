<?php
/**
 * Copyright © 2017 ThemeDimension.com
 */
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Order;
use App\Product;
use App\Size;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    //
    protected function guard()
    {
        return Auth::guard('api');
    }

    public function index() {
        if (!$this->guard()->check()) {
            return response([
                'message' => 'No active user session was found'
            ], 404);
        }
        foreach (Cart::content() as $cart) {
            $data[] = [
                'id' => $cart->id,
                'quantity' => $cart->qty,
                'size_id'   => $cart->options['size_id'],
                'size_name'   => $cart->options['size_name'],
                'row_id' => $cart->rowId
            ];
        }

        return response()->json(['items' => $data], 200);
    }

    public function store(Request $request) {
        session_start();

        if (!$this->guard()->check()) {
            return response([
                'message' => 'No active user session was found'
            ], 404);
        }

        request()->validate([
            'size_id'       => 'required|exists:sizes,id',
            'product_id'    => 'required|exists:products,id',
            'quantity'      => 'required',
        ]);

        $product_price = Product::find($request->product_id)->price;
        $product_size = Size::find($request->size_id)->name;

        Cart::add($request->product_id, 'null', $request->quantity, $product_price, ['size_id' => $request->size_id, 'size_name' => $product_size]);

        // To add same product but different size, modify in vendor 'gloudemans/shoppingcart/src/CartItem.php'

        /*if ($content->has($cartItem->rowId) && $content->has($options['size_id'])) {
            $cartItem = $this->createCartItem($id, $name, $qty, $price, $options);
        }

        if ($content->has($cartItem->rowId)) {
            $cartItem->qty += $content->get($cartItem->rowId)->qty;
        }*/

        foreach (Cart::content() as $cart) {
            $data[] = [
                'id' => $cart->id,
                'quantity' => $cart->qty,
                'size_id'   => $cart->options['size_id'],
                'size_name'   => $cart->options['size_name'],
                'row_id' => $cart->rowId
            ];
        }

        return response()->json(['items' => $data], 200);
    }

    public function update(Request $request) {
        if (!$this->guard()->check()) {
            return response([
                'message' => 'No active user session was found'
            ], 404);
        }
        Cart::update($request->rowId, $request->quantity);

        foreach (Cart::content() as $cart) {
            $data[] = [
                'id' => $cart->id,
                'quantity' => $cart->qty,
                'size_id'   => $cart->options['size_id'],
                'size_name'   => $cart->options['size_name'],
                'row_id' => $cart->rowId
            ];
        }

        return response()->json(['items' => $data], 200);
    }

    public function checkout(Request $request) {
        $order = new Order();
        $user = Auth::user();
        dd($user);
    }
}
