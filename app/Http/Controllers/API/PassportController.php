<?php
/**
 * Copyright © 2017 ThemeDimension.com
 */

/**
 * Created by PhpStorm.
 * User: User
 * Date: 12/11/2017
 * Time: 11:32 AM
 */

namespace App\Http\Controllers\API;

use App\Address;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Validator;

class PassportController extends Controller
{
    public $successStatus = 200;

    public function login() {
        if(Auth::attempt([
            'email' => request('email'),
            'password' => request('password')
        ])) {
            $user = Auth::user();
            $success['id'] = $user->id;
            $success['token'] = $user->createToken('MyApp')->accessToken;

            activity()
                ->useLog('API')
                ->performedOn(Auth::user())
                ->causedBy(Auth::user())
                ->log('Login');

            return response()->json(['success' => $success], $this->successStatus);
        }
        else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function register(Request $request)
    {
        $validatorUser = Validator::make($request->all(), [
            'firstName' => 'required',
            'lastName'  => 'required',
            'phone' => 'required|min:5',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/',
            'c_password' => 'required|same:password',
        ]);

        if($validatorUser->fails()) {
            return response()->json(['error' => $validatorUser->errors()], 401);
        }

        $inputUser = $request->all();
        $inputUser['password'] = bcrypt($inputUser['password']);
        $user = User::create($inputUser);
        $user->roles()->attach('3');

        $validatorAddress = Validator::make($request->all(), [
            'city' => 'required',
            'country' => 'required',
            'district' => 'required',
            'number' => 'required',
            'postalCode' => 'required',
            'street' => 'required',
        ]);

        if($validatorAddress->fails()) {
            return response()->json(['error' => $validatorAddress->errors()], 401);
        }

        $inputAddress = $request->all();
        $inputAddress['user_id'] = $user->id;
        $address = Address::create($inputAddress);

        $success['email'] = $user->email;
        $success['password'] = $user->password;

        return response()->json(['success' => $success], $this->successStatus );
    }

    public function show($id)
    {
        $user = User::where('id',$id)->first();

        $address = User::find($id)->address;
        $data =
            [
                'id' => $user->id,
                'address' => [
                    'city' => $address->city,
                    'country' => $address->country,
                    'district' => $address->district,
                    'number' => $address->number,
                    'postalCode' => $address->postalCode,
                    'street' => $address->street,
                ],
                'firstName' => $user->firstName,
                'lastName' => $user->lastName,
                'email' => $user->email,
                'phone' => $user->phone,
                ];

        $data = collect($data);
        return response()->json(['response' => $data], $this->successStatus);

    }

    protected function guard()
    {
        return Auth::guard('api');
    }

    public function logout(Request $request)
    {
//        dd($request->user('api')->id);
        activity()
            ->useLog('API')
            ->performedOn($request->user('api'))
            ->causedBy($request->user('api')->id)
            ->log('Logout');
        if (!$this->guard()->check()) {
            return response([
                'message' => 'No active user session was found'
            ], 404);
        }

        $request->user('api')->token()->revoke();



        Auth::guard()->logout();
        Session::flush();
        Session::regenerate();

        return response([
            'message' => 'User was logged out'
        ]);
    }

    public function update(Request $request, $id) {
        if (!$this->guard()->check()) {
            return response([
                'message' => 'No active user session was found'
            ], 404);
        }

        $this->validate($request, [
            'id' => $id,
            'firstName' => 'filled',
            'lastName' => 'filled',
            'email' => 'filled|unique:users|email',
            'password' => 'filled',
            'phone' => 'filled',
            'address' => [
                'city' => 'filled',
                'country' => 'filled',
                'district' => 'filled',
                'number' => 'filled',
                'postalCode' => 'filled',
                'street' => 'filled',
            ]
        ]);

        $user = User::find($id);
        $address = User::find($id)->address;

        $data = $request->all();
        $user->fill($data)->save();
        $address->fill($data)->save();

        $response=[
            'id' => $user->id,
            'address' => $address,
            'firstName' => $user->firstName,
            'lastName' => $user->lastName,
            'email' => $user->email,
            'phone' => $user->phone,
        ];

        $response=collect($response);

        return response()->json(['status' => 'User updated successfully', 'response' => $response]);

    }
}