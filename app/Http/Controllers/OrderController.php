<?php

namespace App\Http\Controllers;

use App\Order;
use App\PaymentMethod;
use App\Size;
use App\User;

class OrderController extends Controller
{
    //

    public function index() {
        $orders = Order::all();

        return view('orders.index', compact('orders'));
    }

    public function show($id) {
        $order = Order::find($id);

        $user = User::find($order->user_id);

        $payment_method = PaymentMethod::find($order->payment_method_id)->name;

        return view('orders.show', compact('order', 'user', 'payment_method'));
    }

    public function delete($id) {
        Order::find($id)->delete();

        return redirect()->route('orders.index')->with(
            'success','Order deleted successfully'
        );
    }
}
