<?php
/**
 * Copyright © 2017 ThemeDimension.com
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Models\Activity;

class LogsController extends Controller
{
    //
    public function index()
    {
        $logs = Activity::with('causer')->get();

        return view('logs')->with([
            'logs' => $logs
        ]);
    }
}
