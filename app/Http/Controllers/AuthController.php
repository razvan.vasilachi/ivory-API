<?php
/**
 * Copyright © 2017 ThemeDimension.com
 */

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Laratrust\Traits\LaratrustUserTrait;

class AuthController extends Controller
{
    //
    public function getDashboard()
    {
        if (Auth::check()) {
            return view('admin.dashboard');
        }
        return view('admin.login');
    }

    public function getLogin()
    {
        if (Auth::check() && \Laratrust::hasRole('administrator')) {



            return redirect()->route('admin.dashboard');
        }

        return view('admin.login');
    }

    public function postLogin(Request $request)
    {



        $validatedData = $request->validate([
            'email' => 'required|min:3|max:255|email',
            'password' => 'required',
        ]);

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {

            activity()
                ->useLog('CMS')
                ->performedOn(Auth::user())
                ->causedBy(Auth::user())
                ->log('Login');

            // Authentication passed...
            return redirect()->route('admin.dashboard');
        } else {
            return redirect()->back()->withErrors([
                'password' => 'Your credentials are incorect!'
            ]);
        }
    }

    public function getLogout()
    {
        activity()
            ->useLog('CMS')
            ->performedOn(Auth::user())
            ->causedBy(Auth::user())
            ->log('Logout');
        Auth::logout();
        return redirect()->route('admin.login');
    }

    public function getRegister()
    {
        $users = User::whereHas(
            'roles', function($q){
            $q->where('name', 'administrator');
        }
        )->get();
        if(count($users)>1){
            return redirect()->route('admin.login')->withErrors([
                'hasAdmin' => 'You already have a membership'
            ]);
        }
        return view('admin.register');
    }

    public function postRegister(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required|min:3|max:255|email',
            'password' => 'required|min:6',
            'password_confirmation' => 'required|same:password|min:6'
        ]);

        $checkUser = User::where('email', $request['email'])->first();

        if ($checkUser === null) {
            $user = User::create([
                'firstName' => $request['name'],
                'email' => $request['email'],
                'password' => bcrypt($request['password'])
            ]);

            $user->roles()->attach('2');

            return redirect()->route('admin.dashboard');
        } else {
            return redirect()->back()->withErrors([
                'email' => 'That email address is already registered!'
            ]);
        }
    }
}
