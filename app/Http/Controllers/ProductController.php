<?php
/**
 * Copyright © 2017 ThemeDimension.com
 */

namespace App\Http\Controllers;

use App\Category;
use App\Gender;
use App\Product;
use App\Size;
use App\Variation;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    //

    public function index()
    {
        $products = Product::all();
        return view('products.index')->with([
            'products' => $products
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        $product = Product::find($id);
        $categories   = Category::all();
        $genders      = Gender::all();
        $products     = Product::all();

        return view('products.add-new')->with([
            'categories' => $categories,
            'genders'    => $genders,
            'products'   => $products
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = $request->image;
        $imageName = time().$image->getClientOriginalName();

        request()->validate([
            'category_id'   => 'required',
            'gender_id'     => 'required',
            'name'          => 'required',
            'color'         => 'required',
            'price'         => 'required',
            'image'         => 'mimes:jpg,jpeg,bmp,png',
            'quantity'      => 'required|max:4'
        ]);

        // INSERT PRODUCT
        $product = new Product();
        $product->name          = $request->name;
        $product->category_id   = $request->category_id;
        $product->gender_id     = $request->gender_id;
        $product->color         = $request->color;
        $product->price         = $request->price;
        $product->image         = $imageName;
        if(!empty($request->siblings)) {
            $product->siblings = serialize($request->siblings);
        }
        $product->save();


        // INSERT SIZES
        $sizecount = $request->size_id;

        foreach($sizecount as $key=>$val){
            $sizes [] = [
                'product_id'    => $product->id,
                'name'          => $request->size_id[$key],
                'quantity'      => $request->quantity[$key],
                'created_at'    => \Carbon\Carbon::now(),
                'updated_at'    => \Carbon\Carbon::now()
            ];
        }

        $image->move(config('ivory.upload_dir'),$imageName);

        $sizesInserted = \App\Size::insert($sizes);

        return redirect()->route('product.list')->with(
            'product_added','Product added successfully'
        );
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = Article::find($id);
        return view('articles.show',compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $products   = Product::where('id','!=',$id)->get();
        $categories = Category::all();
        $genders    = Gender::all();
        $product    = Product::find($id);
        $siblings   = $product->siblings;
        if($siblings==null){
            $siblings = array();
        }
        $sizes      = Size::where('product_id',$id)->get();
        return view('products.edit',compact('product','categories','genders','sizes','products','siblings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'name'          => 'required',
            'price'         => 'required',
            'color'         => 'required',
            'image'         => 'mimes:jpg,jpeg,bmp,png',
            'gender_id'     => 'required',
            'category_id'   => 'required',
            'quantity'      => 'required|max:4'
        ]);

        $data = [
            'name'          => $request->input('name'),
            'price'         => $request->input('price'),
            'color'         => $request->input('color'),
            'siblings'      => serialize($request->siblings),
            'gender_id'     => $request->input('gender_id'),
            'category_id'   => $request->input('category_id'),
        ];


        $image = $request->image;
        if($image !== null) {
            $imageName = time().$image->getClientOriginalName();
            $image->move(config('ivory.upload_dir'),$imageName);
            $data['image'] = $imageName;
        }

        Product::find($id)->update($data);

        /********** SIZES **********/

        Size::where('product_id',$id)->delete();

        $allsizes = $request->size_id;
        $sizes = array();
        foreach($allsizes as $key=>$size){
            $sizes [] = [
                'product_id'    => $id,
                'name'          => $request->size_id[$key],
                'quantity'      => $request->quantity[$key],
                'updated_at'    => \Carbon\Carbon::now()
            ];
        }
//
//
//
        $sizesInserted = \App\Size::insert($sizes);

        return redirect()->route('product.list')
            ->with('product_updated','Product updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Product::find($id)->delete();
        return redirect()->route('product.list')->with(
            'success','Product deleted successfully'
        );
//        Article::find($id)->delete();
//        return redirect()->route('articles.index')
//            ->with('success','Article deleted successfully');
    }
}
