<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $fillable = [
      'amount',
      'coupon_code',
      'user_id',
      'payment_method_id',
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function paymentMethod() {
        return $this->belongsTo(PaymentMethod::class);
    }

    public function products() {
        return $this->belongsToMany(Product::class)->withPivot('quantity', 'size_id', 'size_name');
    }

}
