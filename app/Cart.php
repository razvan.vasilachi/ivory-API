<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    //
    protected $fillable = [
        'product_id',
        'size_id',
        'size_name',
        'order_id',
        'quantity',
    ];


}
