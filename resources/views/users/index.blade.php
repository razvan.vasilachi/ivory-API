<!-- Copyright © 2017 ThemeDimension.com -->
@extends('adminlte::page')

@section('title', 'Users')

@include('parts.header')

@section('content_header')
    <h1>Users</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                @if(session()->has('success'))
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i>User deleted</h4>
                    </div>
                @elseif(session()->has('user_updated'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i>User updated</h4>
                    </div>
                @endif
                <div class="box-header with-border">
                    <h3 class="box-title">All users</h3>
                </div>

                <div class="box-body">
                    <table class="table table-bordered all-products">
                        <tbody><tr>
                            <th style="width: 10px">#</th>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Address</th>
                            <th>Edit</th>
                            <th>Remove</th>
                        </tr>
                        @if(!$users->isEmpty())

                            @foreach ($users as $key=>$user)
                                @if(isset($user->address))
                                <tr>
                                    <td>{{ $key }}</td>
                                    <td>{{ $user->firstName }}</td>
                                    <td>{{ $user->lastName }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->phone }}</td>
                                    <td>{{ $user->address->city }}, {{ $user->address->country }}, {{ $user->address->district }}, {{ $user->address->street }}, {{ $user->address->number }}, {{ $user->address->postalCode }}</td>
                                    <td><a href="{{ route('user.edit', [$user->id]) }}"><i class="fa fa-fw fa-edit"></i></a></td>
                                    <td><div data-toggle="modal" data-target="#userModal{{ $user->id }}">
                                            <i class="fa fa-fw fa-remove"></i>
                                        </div>
                                </tr>
                                @endif
                            @endforeach

                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @if(!$users->isEmpty())
        @foreach($users as $user)
        <!-- Modal -->
        <div class="modal modal-danger fade" id="userModal{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Delete user?</h4>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left " data-dismiss="modal">Close</button>
                        <a href="{{ route('user.destroy',[$user->id]) }}" class="btn btn-outline ">Delete</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    @endif
@stop