<!-- Copyright © 2017 ThemeDimension.com -->
@extends('adminlte::page')

@section('title', 'Users')

@include('parts.header')

@section('content_header')
    <div class="col-md-12">
        <h2>Edit user</h2>
    </div>

@stop
@include('parts.header')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <form method="post" action="{{ route('user.update',[6]) }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="put">

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>

                @endif
                <div class="col-sm-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Details</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-sm-3 form-group ">
                                        <label for="name">First Name</label>
                                        <input type="text" name="firstName" class="form-control" id="firstName" placeholder="Enter first name" value="{{$user->firstName}}">
                                    </div>
                                    <div class="col-sm-3 form-group ">
                                        <label for="name">Last Name</label>
                                        <input type="text" name="lastName" class="form-control" id="lastName" placeholder="Enter last name" value="{{$user->lastName}}">
                                    </div>
                                    <div class="col-sm-3 form-group ">
                                        <label for="name">Phone</label>
                                        <input type="text" name="phone" class="form-control" id="phone" placeholder="Enter phone" value="{{$user->phone}}">
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        <label for="price">Email</label>
                                        <input type="text" name="email" class="form-control" id="email" value="{{$user->email}}">
                                    </div>
                                </div>

                                <div>
                                    <h4>Address</h4>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 form-group ">
                                        <label for="name">City</label>
                                        <input type="text" name="city" class="form-control" id="city" placeholder="Enter city" value="{{$user->address->city}}">
                                    </div>
                                    <div class="col-sm-4 form-group ">
                                        <label for="name">Country</label>
                                        <input type="text" name="country" class="form-control" id="country" placeholder="Enter country" value="{{$user->address->country}}">
                                    </div>
                                    <div class="col-sm-4 form-group ">
                                        <label for="name">District</label>
                                        <input type="text" name="district" class="form-control" id="district" placeholder="Enter district" value="{{$user->address->district}}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 form-group ">
                                        <label for="name">Street</label>
                                        <input type="text" name="street" class="form-control" id="street" placeholder="Enter street" value="{{$user->address->street}}">
                                    </div>
                                    <div class="col-sm-4 form-group ">
                                        <label for="name">Number</label>
                                        <input type="number" name="number" class="form-control" id="number" placeholder="Enter number" value="{{$user->address->number}}">
                                    </div>
                                    <div class="col-sm-4 form-group ">
                                        <label for="name">Postal Code</label>
                                        <input type="text" name="postalCode" class="form-control" id="postalCode" placeholder="Enter postalCode" value="{{$user->address->postalCode}}">
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="clearfix"></div>
                        <div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop
