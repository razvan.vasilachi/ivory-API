{{-- resources/views/admin/dashboard.blade.php --}}
<!-- Copyright © 2017 ThemeDimension.com -->
@extends('adminlte::page')

@section('title', 'Dashboard')

@include('parts.header')

@section('content_header')
    <h1>API endpoints</h1>
@stop
@section('content')
    <div class="box">
        <div class="box-body">
            <table class="table table-bordered table-hover logs">
                <tbody>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Method</th>
                    <th>HTTP Request</th>
                    <th>Params</th>
                    <th>Headers</th>
                    <th>Response</th>

                </tr>
                <tr>
                    <td>1</td>
                    <td>POST</td>
                    <td>{{$_SERVER['SERVER_NAME']}}/api/register</td>
                    <td>Email, Password, Confirm Password, First Name, Last Name, Phone, City, Country, District, Number, Postal Code, Street</td>
                    <td>Accept: application/json <br> Content-Type: application/x-www-form-urlencoded</td>
                    <td><img src="../img/register.PNG"></td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>POST</td>
                    <td>{{ url('/api/login') }}</td>
                    <td>Email, Password</td>
                    <td></td>
                    <td><img src="../img/login.PNG"></td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>POST</td>
                    <td>{{ url('/api/logout') }}</td>
                    <td>Email, Password</td>
                    <td>Authorization: Bearer {token} <br> Accept: application/json</td>
                    <td><img src="../img/logout.PNG"></td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>PUT</td>
                    <td>{{ url('/api/users/{id}') }}</td>
                    <td>Fields you want to edit</td>
                    <td>Accept: application/json <br> Authorization: Bearer {token} <br> Content-Type: application/x-www-form-urlencoded</td>
                    <td><img src="../img/update.PNG"></td>
                </tr>
                <tr>
                    <td>5</td>
                    <td>GET</td>
                    <td>{{ url('/api/users/{id}') }}</td>
                    <td></td>
                    <td>Accept: application/json <br> Authorization: Bearer {token}</td>
                    <td><img src="../img/get-user.PNG"></td>
                </tr>
                <tr>
                    <td>6</td>
                    <td>POST</td>
                    <td>{{$_SERVER['SERVER_NAME']}}/api/password/email</td>
                    <td>Email</td>
                    <td>Accept: application/json</td>

                </tr>
                <tr>
                    <td>7</td>
                    <td>POST</td>
                    <td>{{$_SERVER['SERVER_NAME']}}/api/password/reset</td>
                    <td>Email, New Password, Confirm Password, Token</td>
                    <td>Accept: application/json</td>
                </tr>
                <tr>
                    <td>8</td>
                    <td>GET</td>
                    <td>{{$_SERVER['SERVER_NAME']}}/api/products</td>
                    <td></td>
                    <td></td>
                    <td><img src="../img/product.PNG"></td>
                </tr>
                <tr>
                    <td>9</td>
                    <td>GET</td>
                    <td>{{$_SERVER['SERVER_NAME']}}/api/products/{id}</td>
                    <td></td>
                    <td></td>
                    <td><img src="../img/product1.PNG"></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

@stop

@section('js')
    @include('parts.footer');
@stop



