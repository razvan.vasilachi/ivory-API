<!-- Copyright © 2017 ThemeDimension.com -->
@extends('adminlte::page')

@section('title', 'Dashboard')

@include('parts.header')

@section('content_header')

@stop

@section('content')
    <div class="col-sm-12 text-center">
        <h1>Welcome to Ivory Admin Panel</h1>
        <img src="/img/ivory-cover-v2.jpg">
    </div>
    <div class="clearfix"></div>
    <h3 class="text-center" style="margin:30px 0px;">Here are some apps you can integrate with</h3>
    <div class="col-sm-6">
        <a href="https://codecanyon.net/item/ivory-shop-ecommerce-app/20451787?s_rank=1" target="_blank">
            <img src="/img/ios-app.jpg" class="pull-right">
        </a>
    </div>
    <div class="col-sm-6">
        <a href="https://codecanyon.net/item/ivory-shop-ecommerce-app/19807262?s_rank=2" target="_blank">
            <img src="/img/google-app.jpg">
        </a>
    </div>
@stop

