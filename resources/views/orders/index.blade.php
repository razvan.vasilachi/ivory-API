{{-- resources/views/admin/dashboard.blade.php --}}
<!-- Copyright © 2017 ThemeDimension.com -->
@extends('adminlte::page')

@section('title', 'Dashboard')

@include('parts.header')

@section('content_header')
    <h1>Orders</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                @if(session()->has('success'))
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i>Your order has been deleted successfully</h4>
                    </div>
                @endif

                <div class="box-header with-border">
                    <h3 class="box-title">All orders</h3>
                </div>

                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered all-orders">
                        <tbody><tr>
                            <th style="width: 10px">#</th>
                            <th>Order amount</th>
                            <th>Coupon code</th>
                            <th>User name</th>
                            <th>User email</th>
                            <th>User phone</th>
                            <th>User address</th>
                            <th>Payment method</th>
                            <th>Products</th>
                            <th>Remove</th>
                        </tr>
                        @if(!$orders->isEmpty())
                            @foreach ($orders as $key=>$order)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $order->amount }}</td>
                                    <td>{{ $order->coupon_code }}</td>
                                    <td>{{ $order->user->firstName }} {{ $order->user->lastName }}</td>
                                    <td>{{ $order->user->email }}</td>
                                    <td>{{ $order->user->phone }}</td>
                                    <td>{{ $order->user->address->city }}, {{ $order->user->address->country }}, {{ $order->user->address->district }}, {{ $order->user->address->street }}, {{ $order->user->address->number }}, {{ $order->user->address->postalCode }}</td>
                                    <td>{{ $order->paymentMethod->name }}</td>
                                    <td>
                                        <a href="{{route('orders.show', $order->id)}}">View</a>
                                    </td>
                                    <td>
                                        <div data-toggle="modal" data-target="#orderModal{{ $order->id }}">
                                            <i class="fa fa-fw fa-remove"></i>
                                        </div>
                                        <a href="{{ route('orders.delete',[$order->id]) }}/" ></a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody></table>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

    @if(!$orders->isEmpty())
        @foreach($orders as $order)
            <!-- Modal -->
            <div class="modal modal-danger fade" id="orderModal{{ $order->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Delete order?</h4>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline pull-left " data-dismiss="modal">Close</button>
                            <a href="{{ route('orders.delete',[$order->id]) }}" class="btn btn-outline ">Delete</a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endif
@stop

@section('js')
    @include('parts.footer');
@stop



