{{-- resources/views/admin/dashboard.blade.php --}}
<!-- Copyright © 2017 ThemeDimension.com -->
@extends('adminlte::page')

@section('title', 'Dashboard')

@include('parts.header')

@section('content_header')
    <h1>Order detail</h1>
@stop

@section('content')
    <div class="col-sm-12 col-md-12">
        <div class="box box-solid">
            <div class="box-body">
                <h4 style="background-color:#f7f7f7; font-size: 18px; padding: 7px 10px; margin-top: 0;">
                    <b>Name:</b> {{$user->firstName}} {{$user->lastName}} <br>
                    <b>Address:</b> City: {{$user->address->city}} - Country: {{$user->address->country}} - District: {{$user->address->district}} - Number: {{$user->address->number}} - Postal Code: {{$user->address->postalCode}} - Street: {{$user->address->street}} <br>
                    <b>Payment Method: </b> {{$payment_method}} <br>
                    <b>Total: </b>{{$order->amount}}$
                </h4>
                @foreach($order->products as $product)
                    <div class="media">
                        <div class="media-left">
                            <a href="{{route('product.edit', $product->id)}}" class="ad-click-event">
                                <img src="{{asset("uploads/$product->image")}}" alt="{{$product->name}}" class="media-object" style="width: 100px;height: auto;border-radius: 4px;box-shadow: 0 1px 3px rgba(0,0,0,.15);">
                            </a>
                        </div>
                        <div class="media-body">
                            <div class="clearfix">
                                <p class="pull-right">
                                    <a href="{{route('product.edit', $product->id)}}" class="btn btn-success btn-sm ad-click-event">
                                        View product
                                    </a>
                                </p>

                                <h4 style=
                                    "margin-top: 0">{{$product->name}} ─ {{$product->price}}$</h4>

                                <p>{{$product->category->name}} - {{$product->gender->name}} - {{$product->color}}</p>
                                <p style="margin-bottom: 0">
                                    <i class="margin-r5"></i> Size: {{$product->pivot->size_name}}
                                </p>
                                <p style="margin-bottom: 0">
                                    <i class="margin-r5"></i> Quantity: {{$product->pivot->quantity}} buc.
                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection