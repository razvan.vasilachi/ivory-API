{{-- resources/views/admin/dashboard.blade.php --}}
<!-- Copyright © 2017 ThemeDimension.com -->
@extends('adminlte::page')

@section('title', 'Dashboard')

@include('parts.header')

@section('content_header')
    <h1>Logs</h1>
@stop
@section('content')
    <div class="box">
        <div class="box-body">
            <table class="table table-bordered table-hover logs">
                <tbody>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Username</th>
                    <th>Action</th>
                    <th>Details</th>
                    <th>Date</th>
                </tr>
                @foreach($logs as $key=>$log)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $log->causer->firstName }}</td>
                    <td>{{ $log->description }}</td>
                    <td>
                            @if(isset($log->subject->name))
                                {{ $log->subject->name.' - '.$log->subject->color }}
                            @endif
                    </td>
                    <td>{{ $log->created_at->toDateTimeString() }}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@stop

@section('js')
    @include('parts.footer');
@stop



