@section('css')
    <link rel="icon" href="/img/ivory-favicon.PNG" type="image/gif" sizes="16x16">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.3/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/admin/style.css">
@stop

