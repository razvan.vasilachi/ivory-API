{{-- resources/views/admin/dashboard.blade.php --}}
<!-- Copyright © 2017 ThemeDimension.com -->
@extends('adminlte::page')

@section('title', 'Products')

@include('parts.header')

@section('content_header')
    <div class="col-md-12">
        <h2>Add new product</h2>
    </div>

@stop

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <form method="post" action="{{ route('product.add') }}" enctype="multipart/form-data">
                {{ csrf_field() }}

                    @if(!$errors->isEmpty())
                        <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                            @foreach ($errors->all() as $error)
                                <div>{{ $error }}</div>
                            @endforeach
                         </div>
                    @endif

                <div class="col-sm-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Details</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-sm-4 form-group ">
                                        <label for="name">Name</label>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="Enter name" required>
                                    </div>
                                    <div class="col-sm-3 form-group ">
                                        <label for="category">Select category</label>
                                        <select name="category_id" class="form-control" required>
                                            <option value="" disabled selected>Select your option</option>
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        <label for="gender">Gender</label>
                                        <select name="gender_id" class="form-control" id="gender" required>
                                            <option value="" disabled selected>Select your option</option>
                                            @foreach($genders as $gender)
                                                <option value="{{ $gender->id }}">{{ $gender->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 form-group">
                                        <label for="price">Price</label>
                                        <input type="number" name="price" class="form-control" id="price" placeholder="Enter price" required>
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        <div class="form-group">
                                            <label>Color picker:</label>
                                            <input class="form-control" name="color" type="text" id="color" required/>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <img id="myImg" alt="" style="width: 100%;">
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="image">Image</label>
                                                <input class="fullwidth input rqd" type="file" name="image" id="image" accept="image/*" onclick="fileClicked(event)" onchange="fileChanged(event)" required>
                                                <div id="log"></div>
                                            </div>
                                        </div>
                                    </div>

                                </div>


                                <div class="row">
                                    <div class="col-sm-6">
                                        <h4>Variations</h4>
                                        <div class="box-body table-responsive no-padding">
                                            <table id="variationTable" class="table table-bordered table-hover dataTable" role="grid">
                                                <thead>
                                                <tr role="row">
                                                    <th rowspan="1" colspan="1">#</th>
                                                    <th rowspan="1" colspan="1">Size</th>
                                                    <th rowspan="1" colspan="1">Quantity</th>
                                                    <th rowspan="1" colspan="1">Remove</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <tr role="row" class="odd">
                                                    <td>1</td>
                                                    <td>
                                                        <input type="text" name="size_id[]" placeholder="Enter size" required>
                                                    </td>
                                                    <td>
                                                        <input type="number" name="quantity[]" placeholder="Enter quantity" required>
                                                    </td>
                                                    <td>
                                                        <i class="fa fa-fw fa-remove"></i>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <button type="button" class="btn btn-default btn-sm addrow pull-right" style="height: 34px;">
                                            <span class="glyphicon glyphicon-plus-sign"></span> Add
                                        </button>
                                        <div class="clearfix"></div>

                                        <div>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <h4>Siblings</h4>


                                        <div class="form-group">
                                            <select name="siblings[]" class="form-control select2" multiple>
                                                @foreach($products as $product)
                                                    <option value="{{ $product->id }}">{{ $product->name }} ({{ $product->color }})</option>
                                                @endforeach
                                            </select>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </form>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop



@section('js')
    @include('parts.footer');
@stop