{{-- resources/views/admin/dashboard.blade.php --}}
<!-- Copyright © 2017 ThemeDimension.com -->
@extends('adminlte::page')

@section('title', 'Dashboard')

@include('parts.header')

@section('content_header')
    <h1>Products</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                @if(session()->has('success'))
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i>Product deleted</h4>
                    </div>
                @elseif(session()->has('product_added'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i>Your product has been added</h4>
                    </div>
                @elseif(session()->has('product_updated'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i>Your product has been updated</h4>
                    </div>

                @endif

                <div class="box-header with-border">
                    <h3 class="box-title">All products</h3>
                </div>

                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered all-products">
                        <tbody><tr>
                            <th style="width: 10px">#</th>
                            <th class="image"><i class="fa fa-fw fa-image"></i></th>
                            <th>Product name</th>
                            <th>Category</th>
                            <th>Gender</th>
                            <th>Price</th>
                            <th>Edit</th>
                            <th>Remove</th>
                        </tr>
                        @if(!$products->isEmpty())
                            @foreach ($products as $key=>$product)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td class="image"><img src="{{ url(config('ivory.upload_dir').$product->image) }}"></td>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ $product->category->name }}</td>
                                    <td>{{ $product->gender->name }}</td>
                                    <td>{{ $product->price }}</td>
                                    <td><a href="{{ route('product.edit', [$product->id]) }}"><i class="fa fa-fw fa-edit"></i></a></td>
                                    <td><div data-toggle="modal" data-target="#productModal{{ $product->id }}">
                                            <i class="fa fa-fw fa-remove"></i>
                                        </div>
                                        {{--<a href="{{ route('product.destroy',[$product->id]) }}/" ></a></td>--}}
                                </tr>
                            @endforeach
                        @endif



                        </tbody></table>
                </div>

            </div>
            <!-- /.box -->


        </div>
    </div>

    @if(!$products->isEmpty())
        @foreach($products as $product)
        <!-- Modal -->
        <div class="modal modal-danger fade" id="productModal{{ $product->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Delete product?</h4>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left " data-dismiss="modal">Close</button>
                        <a href="{{ route('product.destroy',[$product->id]) }}" class="btn btn-outline ">Delete</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    @endif
@stop

@section('js')
    @include('parts.footer');
@stop



