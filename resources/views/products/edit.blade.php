{{-- resources/views/admin/dashboard.blade.php --}}
<!-- Copyright © 2017 ThemeDimension.com -->
@extends('adminlte::page')

@section('title', 'Products')

@include('parts.header')

@section('content_header')
    <div class="col-md-12">
        <h2>Edit product</h2>
    </div>

@stop
@include('parts.header')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <form method="post" action="{{ route('product.update',[$product->id]) }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="put">

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="col-sm-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Details</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-sm-4 form-group ">
                                        <label for="name">Name</label>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="Enter name" value="{{$product->name}}">
                                    </div>
                                    <div class="col-sm-3 form-group ">
                                        <label for="category">Select category</label>
                                        <select name="category_id" class="form-control">
                                            @foreach ($categories as $category)
                                                <option value="{{ $category->id }}"
                                                @if( $product->category->id == $category->id )
                                                    {{" selected"}}
                                                        @endif>
                                                    {{ $category->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-3 form-group ">
                                        <label for="gender">Select gender</label>
                                        <select name="gender_id" class="form-control">
                                            @foreach ($genders as $gender)
                                                <option value="{{ $gender->id }}"
                                                @if( $product->gender->id == $gender->id )
                                                    {{" selected"}}
                                                        @endif>
                                                    {{ $gender->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4 form-group">
                                        <label for="price">Price</label>
                                        <input type="number" name="price" class="form-control" id="price" value="{{$product->price}}">
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        <label for="color">Color</label>
                                        <input class="form-control" name="color" type="text" id="color" value="{{$product->color}}"/>
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <img id="myImg" src="../../../uploads/{{$product->image}}" style="width: 100%;">
                                            </div>
                                            <div class="col-xs-6">
                                                <label for="image">Image</label>
                                                <input class="fullwidth input rqd" type="file" name="image" id="image" accept="image/*" onclick="fileClicked(event)" onchange="fileChanged(event)">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <h4>Variations</h4>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <table id="variationTable" class="table table-bordered table-hover dataTable" role="grid">
                                            <thead>
                                            <tr role="row">
                                                <th rowspan="1" colspan="1">#</th>
                                                <th rowspan="1" colspan="1">Size</th>
                                                <th rowspan="1" colspan="1">Quantity</th>
                                                <th rowspan="1" colspan="1">Remove</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($sizes as $size)
                                                <tr role="row" class="odd">
                                                    <td>1</td>
                                                    <td>
                                                        <input type="text" name="size_id[]" value="{{$size->name}}">
                                                    </td>
                                                    <td>
                                                        <input type="number" name="quantity[]" value="{{ $size->quantity }}">
                                                    </td>
                                                    <td>
                                                        <i class="fa fa-fw fa-remove"></i>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        <button type="button" class="btn btn-default btn-sm addrow pull-right" style="height: 34px;">
                                            <span class="glyphicon glyphicon-plus-sign"></span> Add
                                        </button>
                                        <div class="clearfix"></div>
                                        <div>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <h4>Siblings</h4>


                                        <div class="form-group">
                                            <select name="siblings[]" class="form-control select2" multiple>
                                                @foreach($products as $product)
                                                    <option value="{{ $product->id }}"

                                                    @if(in_array($product->id, $siblings))
                                                        selected
                                                    @endif
                                                    >{{ $product->name }} ({{ $product->color }})</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </form>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop



@section('js')
    @include('parts.footer');
@stop
